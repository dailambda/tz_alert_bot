module Option = struct
  let pp f fmt = function
    | None -> Format.fprintf fmt "None"
    | Some x -> Format.fprintf fmt "@[<2>Some (@[%a@])@]" f x
end
