open Misc
open Telegram.Api

let (>>=) = Lwt.bind
let (>|=) = Lwt.Infix.(>|=)

module User = struct
  include User
  let pp fmt u = Format.fprintf fmt "id=%d name=%s" u.id u.first_name
end

module MyBot = Mk (struct
  open Chat
  open User
  open Command
  open Message
  open UserProfilePhotos

  include Telegram.BotDefaults

  let token =
    match Sys.getenv "HOME" with
    | exception Not_found -> failwith "Need $HOME"
    | home ->
        let fn = home ^ "/.tz_alert_bot_token" in
        let ic = match open_in fn with
          | exception _ ->
              failwith (Printf.sprintf "Need file %s carrying the api token" fn)
          | ic -> ic
        in
        let s = input_line ic in
        close_in ic;
        s

  (* Can be replaced with whatever the bot's name is, makes the bot only respond to /say_hi@mlbot *)
  let command_postfix = Some "tz_alert_bot"

  let commands =
    let open Telegram.Actions in
    let say_hi {chat = {id; _}; _} =
      Lwt.return @@ send_message ~chat_id:id "Hi"
    in
    let my_pics = function
      | {chat; from = Some {id; _}; _} ->
          Lwt.return @@ get_user_profile_photos id
          /> begin function
            | Result.Success photos ->
                send_message ~chat_id:chat.id "Your photos: %d" photos.total_count
            | Result.Failure _ ->
              send_message ~chat_id:chat.id "Couldn't get your profile pictures!"
          end
      | {chat = {id; _}; _} ->
          Lwt.return @@ send_message ~chat_id:id "Couldn't get your profile pictures!"
    and check_admin {chat = {id; _}; _} =
      send_message ~chat_id:id "Congrats, you're an admin!"
    in
    [{name = "say_hi"; description = "Say hi!"; enabled = true; run = say_hi};
     {name = "my_pics"; description = "Count profile pictures"; enabled = true; run = my_pics};
     {name = "admin"; description = "Check whether you're an admin"; enabled = true; run = fun x -> Lwt.return @@ with_auth ~command:check_admin x}]

  let callback _q = Command.Nothing
end)

let new_chat_members = Hashtbl.create 1000

let gc_new_chat_members mes =
  let date = mes.Message.date in
  let olds =
    Hashtbl.fold (fun k v olds ->
        if date - v > 60 * 60 * 24 * 7 (* 1 week *) then k::olds
        else olds) new_chat_members []
  in
  List.iter (fun old -> Hashtbl.remove new_chat_members old) olds

let register_new_chat_member mes =
  match mes.Message.new_chat_member with
  | None -> ()
  | Some user ->
      Format.eprintf "New chat member: %a@." User.pp user;
      Hashtbl.replace new_chat_members user.id mes.date

(* Bot never receive the messages by itself I believe *)
let check =
  let p1 = Re.Pcre.regexp ~flags:[`CASELESS] "tezbox|gal+eon|kukai" in
  let p2 = Re.Pcre.regexp ~flags:[] "ICO" in
  let p_fake_wallet = Re.Pcre.regexp ~flags:[] "kkukai" in
  let last_warn_time = ref 0 in
  fun chat mes ->
    let direct_chat_with_bot =
      chat.Chat.id = 663728409 (* not the direct chat with the bot *)
    in
    match mes.Message.from with
    | None -> None (* not interested in messages w/o user *)
    | Some user ->
        let text = match mes.Message.text with None -> "" | Some x -> x in
        if
          (* fake wallet *)
          Re.Pcre.pmatch ~rex:p_fake_wallet text
        then
          Some `Fake_wallet
        else
          let is_new_chat_member = Hashtbl.mem new_chat_members user.User.id in
          if
            direct_chat_with_bot
            || is_new_chat_member
            || mes.Message.date - !last_warn_time > 60 * 60 * 24 (* 1 day *)
          then
            let match_ =
              Re.Pcre.pmatch ~rex:p1 text
              || Re.Pcre.pmatch ~rex:p2 text
            in
            if not match_ then None
            else begin
              if not direct_chat_with_bot && match_ then
                last_warn_time := mes.Message.date;
              (* If warned, the member is no longer a new chat member *)
              if match_ then Hashtbl.remove new_chat_members user.User.id;
              Some (if direct_chat_with_bot || is_new_chat_member
                    then `Newbie_warning
                    else `Oldie_warning)
            end
          else
            None

let load_txt fn =
  let ic = open_in fn in
  let buf = Buffer.create 1024 in
  let len = 1024 in
  let bytes = Bytes.create len in
  let rec f () =
    let read = input ic bytes 0 len in
    if read = 0 then ()
    else begin
      Buffer.add_subbytes buf bytes 0 read;
      f ()
    end
  in
  f ();
  close_in ic;
  Buffer.contents buf

(* multi map *)
let last_messages = Hashtbl.create 3

let remove_old_messages ~chat_id =
  let message_ids = Hashtbl.find_all last_messages chat_id in
  Lwt_list.iter_s (fun message_id ->
      Format.eprintf "delete chat_id:%d message_id:%d@." chat_id message_id;
      MyBot.delete_message ~chat_id ~message_id >|= ignore) message_ids >|= fun () ->
  List.iter (fun _message_id -> Hashtbl.remove last_messages chat_id) message_ids

let do_update = function
  | Update.Message (n, mes) ->
      gc_new_chat_members mes;
      register_new_chat_member mes;
      let chat = mes.chat in
      Format.eprintf "%.0f: message chat id:%d update:%d user:%a time: %d %a@."
        (Unix.time ())
        chat.id
        n
        (Option.pp User.pp) mes.from
        mes.date
        (Option.pp Format.pp_print_string) mes.text;
      begin match check chat mes with
      | None -> Lwt.return ()
      | Some k ->
          MyBot.send_message ~chat_id:chat.id
            ~text:( load_txt
                      (match k with
                       | `Newbie_warning -> "long.txt"
                       | `Oldie_warning -> "short.txt"
                       | `Fake_wallet -> "fake_wallet.txt") )
            ~parse_mode:(Some Markdown)
            ~disable_web_page_preview:true
            ~reply_to:None
            ~reply_markup: None
            ~disable_notification:false
          >>= function
          | Success j ->
              remove_old_messages ~chat_id:chat.id >|= fun () ->
              begin match j with
              | `Assoc kvs ->
                  begin match List.assoc_opt "message_id" kvs with
                  | Some (`Int message_id) ->
                      Format.eprintf "chat_id: %d message_id: %d@." chat.id message_id;
                      Hashtbl.add last_messages chat.id message_id
                  | _ -> ()
                  end
              | _ -> ()
              end
          | Failure e ->
              Format.eprintf "send_message failed: %s@." e;
              Lwt.return ()
      end
  | Other (id, json) ->
      Format.eprintf "%d: other %a@." id Yojson.Safe.pp json; Lwt.return ()

  | EditedMessage _ ->
      prerr_endline "EditedMessage"; Lwt.return ()
  | ChannelPost _ ->
      prerr_endline "ChannelPost"; Lwt.return ()
  | EditedChannelPost _ ->
      prerr_endline "EditedChannelPost"; Lwt.return ()
  | InlineQuery _ ->
      prerr_endline "InlineQuery"; Lwt.return ()
  | ChosenInlineResult _ ->
      prerr_endline "ChosenInlineResult"; Lwt.return ()
  | CallbackQuery _ ->
      prerr_endline "CallbackQuery"; Lwt.return ()

let run ?(log=true) () =
  let process = function
    | Result.Success update ->
        prerr_endline "update";
        do_update update
    | Result.Failure "Result is empty {\"ok\":true,\"result\":[]}" ->
        (* Silly, but it requires lots of code change of TelegaML to fix tihs *)
        Lwt.return ()
    | Result.Failure e ->
        if log && e <> "Could not get head" then (* Ignore spam *)
          Lwt_io.printl e
      else Lwt.return ()
  in
  let _test () =
    MyBot.get_chat ~chat_id:(-1001353494616) >>= function
    | Success x ->
        Format.eprintf "Chat: %d %a@." x.id (Option.pp Format.pp_print_string) x.title;
        Lwt.return ()
    | Failure s -> Format.eprintf "get_chat: %s@." s; Lwt.return ()
  in
  let rec loop () =
    MyBot.pop_update ~run_cmds:true () >>= process >>= loop in
  while true do (* Recover from errors if an exception is thrown *)
    try Lwt_main.run @@ loop ()
    with e ->
      Printexc.print_backtrace stderr;
      Format.eprintf "Error: %s@." (Printexc.to_string e); ()
  done

let () = run ()
